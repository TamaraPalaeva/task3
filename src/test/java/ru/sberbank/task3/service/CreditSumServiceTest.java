package ru.sberbank.task3.service;

import org.junit.Assert;
import org.junit.Test;
import ru.sberbank.task3.entity.Client;
import ru.sberbank.task3.entity.Credit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CreditSumServiceTest {

    @Test
    public void summaCredit() {
        List<Credit> creditTest = new ArrayList<>();

        creditTest.add(new Credit(new Client("Палаева", "Анастасия"), 200L));
        creditTest.add(new Credit(new Client("Палаева", "Анастасия"), 600L));
        creditTest.add(new Credit(new Client("Норкин", "Иван"), 1500L));

        Map<String, Long> testMap = new HashMap<>();
        testMap.put("Палаева Анастасия", 800L);
        testMap.put("Норкин Иван", 1500L);

        Map<String, Long> creditTestMap = new CreditSumService().summaCredit(creditTest);

        Object a = new Object();
        Object b = new Object();
        a.equals(b);


        Assert.assertEquals(testMap,creditTestMap);

    }
}
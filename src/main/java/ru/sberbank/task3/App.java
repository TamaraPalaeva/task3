package ru.sberbank.task3;

import ru.sberbank.task3.entity.Client;
import ru.sberbank.task3.entity.Credit;

import java.util.*;

public class App {

   public static List<Client> clients = new ArrayList<>();
   public static List<Credit> credits = new ArrayList<>();
   public static Map<String,Long> summsCredit = new HashMap<>();

    public static void main(String[] args) {

        clients.add(new Client("Палаева", "Тамара"));
        clients.add(new Client("Буров", "Иван"));
        clients.add(new Client("Зобрин","Владимир"));


        credits.add(new Credit(clients.get(0) , 2000L));
        credits.add(new Credit(clients.get(2) , 15000L));
        credits.add(new Credit(clients.get(1) , 12000L));
        credits.add(new Credit(clients.get(2) , 5000L));
        credits.add(new Credit(clients.get(1) , 9000L));
        credits.add(new Credit(clients.get(1) , 6000L));

         System.out.println("Oбщая сортировка");
        Collections.sort(credits, new Credit.CompereCredit());
        for (Credit credit :  credits) {
            System.out.println(credit);
        }

        System.out.println("\n\n Сортировка по Имени");

        Collections.sort(credits, new Credit.CompereCreditToClient());
        for (Credit credit :  credits) {
            System.out.println(credit);
        }

        System.out.println("\n\n Сортировка по сумме");

        Collections.sort(credits, new Credit.CompereCreditToSumma());
        for (Credit credit :  credits) {
            System.out.println(credit);
        }

        System.out.println("\n\n Сумма кредитов у клиента");
        summsCredit = new Credit().summaCredit(credits);
        for (Map.Entry<String, Long> entry : summsCredit.entrySet()) {
            System.out.println(entry.getKey() + " : " + (entry.getValue()/100 + "." + (entry.getValue() %100)));
        }



        }




}

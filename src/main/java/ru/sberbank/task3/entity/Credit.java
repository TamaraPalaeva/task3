package ru.sberbank.task3.entity;

import ru.sberbank.task3.service.CreditSumService;

import java.util.*;

public class Credit extends CreditSumService {
    private Client client;
    private  Long summa;

    public Credit() {

    }

    public Credit(Client client, Long summa) {
        this.client = client;
        this.summa = summa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return Objects.equals(client, credit.client) &&
                Objects.equals(summa, credit.summa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, summa);
    }

    @Override
    public String toString() {
        return client.toString() + " : " + summa/100 + "." + (summa%100) + "руб";
    }

    public Client getClient() {
        return client;
    }

    public Long getSumma() {
        return summa;
    }

    @Override
    public Map<String, Long> summaCredit(List<Credit> credits) {
        return super.summaCredit(credits);
    }

    public static class CompereCreditToClient implements Comparator<Credit>{

        @Override
        public int compare(Credit o1, Credit o2) {
            String client1 = o1.getClient().toString();
            String client2 = o2.getClient().toString();
            return client1.compareTo(client2);
        }
    }

    public static class CompereCreditToSumma implements Comparator<Credit>{

        @Override
        public int compare(Credit o1, Credit o2) {
            return (int) (o1.summa - o2.summa);
        }
    }
    public static class CompereCredit implements Comparator<Credit>{

        @Override
        public int compare(Credit o1, Credit o2) {
            String client1 = o1.getClient().toString();
            String client2 = o2.getClient().toString();
            int nameCompareResult = client1.compareTo(client2);
            if (nameCompareResult == 0){
                return (int)(o1.summa - o2.summa);
            }
            return nameCompareResult;
        }
    }

}

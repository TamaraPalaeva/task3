package ru.sberbank.task3.service;

import ru.sberbank.task3.entity.Credit;

import java.util.*;

public class CreditSumService {

    public Map<String, Long> summaCredit(List<Credit> credits) {
        Map<String, Long> summaCredits = new HashMap<>();
        for (int i = 0; i < credits.size(); i++) {
            long sum = credits.get(i).getSumma();
            String name = credits.get(i).getClient().toString();
            if(!summaCredits.containsKey(name)){
                for (int j = 0; j < credits.size(); j++) {
                    String n = credits.get(j).getClient().toString();
                    if ((i != j) && (name.equals(n))){
                        sum += credits.get(j).getSumma();
                    }
                }
                summaCredits.put(name, sum);
            }
        }
        return summaCredits;
    }
}
